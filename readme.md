# Wallpaper changer for Unix systems

## TODO:
1. [x] GUI
2. [ ] Add Google Pictures search via API
3. [ ] Add Steam Workshop (Wallpaper engine) wallpapers support (image/video)
4. [ ] Add generating (AI) images via API 
5. [ ] Create engine for displaying custom vector graphics wallpapers
6. [ ] Check this program on other system's:
   1. [x] Ubuntu 22.04
   2. [x] MacOS Sonoma
   3. [ ] other systems