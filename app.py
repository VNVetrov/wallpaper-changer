import json
import os
import platform
import subprocess
import tkinter

import PIL.Image
import customtkinter
import requests

SCALING_FACTOR = 1.3
MEDIA_ROOT = "media"

customtkinter.set_appearance_mode("dark")
customtkinter.set_window_scaling(SCALING_FACTOR)
customtkinter.set_widget_scaling(SCALING_FACTOR)


def load_cfg():
    if not os.path.exists('config.json'):
        with open('config.json', 'w') as f:
            json.dump({}, f)
        return {}
    with open('config.json', 'r') as f:
        return json.load(f)


def save_cfg(cfg):
    with open('config.json', 'w') as f:
        json.dump(cfg, f)


def config_handler(func):
    def wrapper(*args, **kwargs):
        cfg = load_cfg()
        result = func(*args, config=cfg, **kwargs)
        save_cfg(cfg)
        return result

    return wrapper


def set_wallpaper(absolute_image_path):
    system_type = get_system_type()
    system_color_scheme = get_system_color_scheme()

    if not os.path.isfile(absolute_image_path) and os.path.exists(absolute_image_path):
        return
    if system_type == 'darwin':
        command = 'osascript -e \'tell application \"Finder\" to set desktop picture to POSIX file \"{}\"\''.format(
            absolute_image_path)
        run_command(command)
    else:
        command = "gsettings set org.gnome.desktop.background picture-uri{} file://{}"
        if system_color_scheme == "\'prefer-dark\'":
            run_command(command.format('-dark', absolute_image_path))
        else:
            run_command(command.format('', absolute_image_path))


def download_image(image_url):
    # creating needed folders
    if not os.path.exists(MEDIA_ROOT):
        os.makedirs(MEDIA_ROOT, exist_ok=True)

    # getting image path
    image_path = os.path.join(MEDIA_ROOT, os.path.basename(image_url))
    absolute_image_path = get_absolute_image_path(image_path)

    # downloading image
    if not os.path.exists(absolute_image_path):
        response = requests.get(image_url)
        response.raise_for_status()

        image_data = response.content

        with open(absolute_image_path, 'wb') as handler:
            handler.write(image_data)

    return absolute_image_path


def get_absolute_image_path(relative_path):
    current_dir = os.getcwd()

    absolute_path = os.path.join(current_dir, relative_path)

    return absolute_path


def run_command(command):
    print("Running command: {}".format(command))
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)

    output, error = process.communicate()

    if error:
        print("Error:", error.decode())

    print('Output: {}'.format(output.decode()))

    return output.decode()


@config_handler
def get_system_color_scheme(*args, **kwargs):
    config = kwargs.get('config')
    # checking system color-scheme (for Ubuntu 22.04+)
    if config['system_type'] == 'linux':
        system_color_scheme = config.get('system_color_scheme')
        if not system_color_scheme:
            system_color_scheme = run_command(
                'gsettings get org.gnome.desktop.interface '
                'color-scheme').strip('\n')
            config['system_color_scheme'] = system_color_scheme
        return system_color_scheme


@config_handler
def get_system_type(*args, **kwargs):
    config = kwargs.get('config')
    # checking system type (Darwin, Ubuntu...)
    system_type = config.get('system_type')
    if not system_type:
        system_type = platform.system().lower()
        config['system_type'] = system_type

    return system_type


def get_previous_wallpapers():
    previous_wallpapers = []
    files = os.listdir(MEDIA_ROOT)
    allowed_formats = ('png', 'jpg', 'jpeg', 'gif', 'bmp')
    for file in files:
        if file.endswith(allowed_formats):
            previous_wallpapers.append({'image_path': get_absolute_image_path(os.path.join(MEDIA_ROOT, file))})
    return previous_wallpapers


@config_handler
def get_current_wallpaper_path(*args, **kwargs):
    config = kwargs.get('config')
    if config.get("system_type") == 'linux':
        command = 'gsettings get org.gnome.desktop.background picture-uri{}'
        if config.get("system_color_scheme") == "\'prefer-dark\'":
            return run_command(command.format('-dark')).strip('\n').strip('\'').strip('file:')
        else:
            return run_command(command.format('')).strip('\n').strip('\'').strip('file:')


class MainWindow(customtkinter.CTk):
    def __init__(self):
        super().__init__()
        self.title("Wallpaper changer")
        width = self.winfo_screenwidth()
        height = self.winfo_screenheight()
        self.geometry("%dx%d" % (width, height))
        self.minsize(1000, 700)

        self.wallpaper_frame = WallpapersFrame(master=self)
        self.wallpaper_frame.pack(side=tkinter.LEFT, fill=tkinter.BOTH, expand=True, padx=5, pady=10, ipadx=100)

        self.tab_view = TabView(master=self)
        self.tab_view.pack(side=tkinter.RIGHT, fill=tkinter.BOTH, expand=True, padx=5, pady=10, ipadx=400)


class TabView(customtkinter.CTkTabview):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.image_url_tab = self.add("Image URL")
        self.google_pictures = self.add("Google Pictures")
        self.steam_workshop = self.add("Steam Workshop")
        self.AI_generation = self.add("AI generation")
        self.set("Image URL")
        self.image_url_frame = ImageURLFrame(self.image_url_tab, border_width=1, border_color='gray')
        self.image_url_frame.pack(expand=True, ipadx=250, ipady=500)


class WallpapersFrame(customtkinter.CTkFrame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.current_wallpaper_label = customtkinter.CTkLabel(master=self, text="Current Wallpaper")
        self.current_wallpaper_label.pack()

        current_wallpaper_path = get_current_wallpaper_path()
        self.current_wallpaper_frame = WallpaperFrame(master=self, image_path=current_wallpaper_path)
        self.current_wallpaper_frame.pack()

        self.previous_wallpapers_label = customtkinter.CTkLabel(master=self, text="Previous Wallpapers:")
        self.previous_wallpapers_label.pack()

        self.previous_wallpapers = get_previous_wallpapers()
        if self.previous_wallpapers:
            for previous_wallpaper in self.previous_wallpapers:
                previous_wallpaper_frame = WallpaperFrame(master=self,
                                                          image_path=previous_wallpaper['image_path'])
                previous_wallpaper_frame.pack(expand=True)


class WallpaperFrame(customtkinter.CTkFrame):
    def __init__(self, master, image_path, **kwargs):
        super().__init__(master, **kwargs)

        self.image_path = image_path
        self.image = PIL.Image.open(image_path)
        self.image_ctk = customtkinter.CTkImage(size=(500, 300), dark_image=self.image)

        self.image_button = customtkinter.CTkButton(self, image=self.image_ctk, font=('', 16), text='',
                                                    corner_radius=10, fg_color='grey28', command=self.set_wallpaper)
        self.image_button.pack()

    def set_wallpaper(self):
        set_wallpaper(self.image_path)


class ImageURLFrame(customtkinter.CTkFrame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.configure(fg_color=("gray78", "gray28"))

        self.label = customtkinter.CTkLabel(self, text="Image URL", font=('', 24))
        self.label.pack(fill=tkinter.X, expand=True, padx=50)

        self.image_url_input = customtkinter.CTkEntry(self, placeholder_text="Enter image url", height=75,
                                                      font=('', 16))
        self.image_url_input.pack(fill=tkinter.X, expand=True, padx=100)

        self.submit_button = customtkinter.CTkButton(master=self, text="Set wallpaper", command=self.set_wallpaper,
                                                     width=250, font=('', 16))
        self.submit_button.pack(expand=True, padx=50)

    def set_wallpaper(self):
        image_url = self.image_url_input.get()

        absolute_image_path = download_image(image_url)

        set_wallpaper(absolute_image_path)


if __name__ == "__main__":
    main_window = MainWindow()
    main_window.mainloop()
